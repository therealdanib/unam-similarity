<?php

namespace GIL\AppSimilitudBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Docxrepo
 *
 * @ORM\Table(name="DocxRepo", indexes={@ORM\Index(name="fk_Repositorio_has_Documento_Documento1_idx", columns={"Documento_idDocumento"}), @ORM\Index(name="fk_Repositorio_has_Documento_Repositorio_idx", columns={"Repositorio_idRepositorio"})})
 * @ORM\Entity
 */
class Docxrepo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="iddocxrepo", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddocxrepo;

    /**
     * @var integer
     *
     * @ORM\Column(name="Repositorio_idRepositorio", type="integer", nullable=false)
     */
    private $repositoriorepositorio;

    /**
     * @var integer
     *
     * @ORM\Column(name="Documento_idDocumento", type="integer", nullable=false)
     */
    private $documentodocumento;


    /**
     * Get iddocxrepo
     *
     * @return integer 
     */
    public function getIddocxrepo()
    {
        return $this->iddocxrepo;
    }

    /**
     * Set repositoriorepositorio
     *
     * @param integer $repositoriorepositorio
     * @return Docxrepo
     */
    public function setRepositoriorepositorio($repositoriorepositorio)
    {
        $this->repositoriorepositorio = $repositoriorepositorio;

        return $this;
    }

    /**
     * Get repositoriorepositorio
     *
     * @return integer
     */
    public function getRepositoriorepositorio()
    {
        return $this->repositoriorepositorio;
    }

    /**
     * Set documentodocumento
     *
     * @param integer
     * @return Docxrepo
     */
    public function setDocumentodocumento($documentodocumento)
    {
        $this->documentodocumento = $documentodocumento;

        return $this;
    }

    /**
     * Get documentodocumento
     *
     * @return integer
     */
    public function getDocumentodocumento()
    {
        return $this->documentodocumento;
    }
}
