<?php

namespace GIL\AppSimilitudBundle\Controller;

use GIL\AppSimilitudBundle\Entity\Documento;
use GIL\AppSimilitudBundle\Entity\Docxrepo;
use GIL\AppSimilitudBundle\Entity\Repositorio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CrearController extends Controller {


    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @throws InternalErrorException
     */

    public function indexAction(Request $request) {

        $id = $request->query->get('pid');
        $iframe = $request->query->get('embed');

        Utils::getLogin($request);
        $session_id = $request->cookies->get('session_id');

        $project = $this->getProject($session_id, $id);

        if(!$project){

            throw new NotFoundHttpException('No existe el proyecto especificado');
        }
        $files = $this->getProjectFiles($session_id,$project)['result']['documents'];

        return $this->render('GILAppSimilitudBundle:Default:create.html.twig',array(
            'iframe' => $iframe,
            'project' => $project,
            'files' => $files,
            'idrepo' => $id,
            'metadata_headers'=> array_keys($files[0]['metadata']),
            'exists' => $this->repoExists($id)
        ));
    }

    /**
     * @param Request $request
     * @param $idrepo
     * @return Response
     * @throws InternalErrorException
     */

    public function calculateSimilarityAction(Request $request, $idrepo){

        Utils::getLogin($request);
        $session_id = $request->cookies->get('session_id');
        $project = $this->getProject($session_id, $idrepo);

        $files = $this->getProjectFiles($session_id,$project)['result']['documents'];

        $file_ids = array_column($files,'id');
        $data = $this->downloadZip($file_ids, $session_id);
        $data = base64_decode($data->result->datas);

        if(!$data){

            $response = new JsonResponse();
            $response->setData(array(
                'code'=>'500',
                'title' => 'Error de comunicación con la API',
                'description' => 'No fue posible descargar los archivos de la API de GECO',
                'params' => ''
            ));
            return $response;
        }

        $zipName = 'tmp_'.$idrepo.'_'.$session_id.'.zip';
        $filename = $this->get('kernel')->getRootDir() . '/../web/files/'.$zipName;
        file_put_contents($filename, $data);
        chmod($filename, 0777);

        $fsize = filesize($filename);

        $this->deleteRepo($idrepo);
        $this->insertRepo($project,$files);
        $pid = $this->executeJar($this->get('kernel')->getRootDir().'/../web/files/',$zipName,$project['id'], $fsize);

        if(isset($pid[0])){

            $this->actualizaEstadoProyecto($pid[0],$project['id']);
            $response = new JsonResponse();
            $response->setData(array(
                'code'=>'200',
                'title' => 'Proceso ejecutado',
                'description' => 'El proceso de similitud para este proyecto se ha ejecutado con el Id '.$pid[0].', para ver el estado del proceso, ingresa a consultar el proyecto',
                'params' => $pid
            ));
        }
        else{

            $this->deleteRepo($idrepo);
            $response = new JsonResponse();
            $response->setData(array(
                'code'=>'500',
                'title' => 'Error',
                'description' => 'Algo salió mal, por favor intenta reiniciar el proceso',
                'params' => ''
            ));
        }

        return $response;
    }

    /**
     * @param $session_id
     * @param $project
     * @return mixed
     */

    private function getProjectFiles($session_id, $project) {

        $data = array
        (
            'jsonrpc' => '2.0',
            'method' => 'call',
            'params' => array(

                'project_id' => $project['id']
            ),
        );

        $payload= json_encode($data);

        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,Utils::PARENT_URL.'/api/project/documents?session_id='.$session_id);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_HTTPHEADER,array
        (
            'content-type: application/json',
        ));
        curl_setopt($curl, CURLOPT_POSTFIELDS,$payload);
        $response = curl_exec($curl);
        curl_close($curl);
        $datas = $response;

        return json_decode($datas, true);
    }

    /**
     * @param $session_id
     * @param $project_id
     * @return null
     */

    private function getProject($session_id, $project_id) {

        $project_url = Utils::PARENT_URL.'/api/project?session_id='.$session_id;
        $response = json_decode(file_get_contents($project_url, false), true);

        if($response){

            foreach ($response as $r){

                if($r['id'] == $project_id){

                    return $r;
                }
            }
        }
        return null;
    }

    /**
     * @param $file_ids
     * @return mixed
     */

    private function downloadZip($file_ids, $session_id){

        $data = array
        (
            'jsonrpc' => '2.0',
            'method' => 'call',
            'params' => array(

                'doc_ids' => $file_ids,
                'same_dir' => true,
                'processor_id' => 1,
                'copyright' => false
            ),
        );

        $payload= json_encode($data);

        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,Utils::PARENT_URL.'/api/directory/download?session_id='.$session_id);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_HTTPHEADER,array
        (
            'content-type: application/json',
        ));
        curl_setopt($curl, CURLOPT_POSTFIELDS,$payload);
        $response = curl_exec($curl);
        curl_close($curl);

        $datas = json_decode($response);

        return $datas;
    }

    /**
     * @param $repo
     * @param $documentos
     * @throws InternalErrorException
     */

    private function insertRepo($repo, $documentos){

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        try{

            $repoDB = new Repositorio();

            if(isset($repo['description'])){

                $repoDB->setDescripcionrepositorio($repo['description']);
            }
            else $repoDB->setDescripcionrepositorio("No existe una descripción para este proyecto");

            $repoDB->setIdrepositorio($repo['id']);
            $repoDB->setProcesado(0);
            $repoDB->setTitulorepositorio($repo['name']);

            $em->persist($repoDB);
            $em->flush();

            foreach ($documentos as $d) {

                $docDB = $this->getDoctrine()->getRepository('GILAppSimilitudBundle:Documento')->find($d['id']);

                if(!$docDB){

                    $docDB = new Documento();
                    $docDB->setIddocumento($d['id']);
                    $docDB->setTitulodocumento(!isset($d['metadata']['titulo']) ? $d['name'] : $d['metadata']['titulo']);
                    $docDB->setDescripciondocumento(json_encode($d['metadata']));

                    $em->persist($docDB);
                    $em->flush();

                }else{

                    $docDB->setTitulodocumento(!isset($d['metadata']['titulo']) ? $d['name'] : $d['metadata']['titulo']);
                    $docDB->setDescripciondocumento(json_encode($d['metadata']));
                    $em->flush();
                }

                $dxr = new Docxrepo();
                $dxr->setDocumentodocumento($docDB->getIddocumento());
                $dxr->setRepositoriorepositorio($repoDB->getIdrepositorio());
                $em->persist($dxr);
                $em->flush();

            }

            $em->getConnection()->commit();

        } catch (\Exception $ex){

            $em->getConnection()->rollback();
            throw new InternalErrorException($ex->getMessage());
        }
    }

    private function executeJar($path,$zip,$project_id){

        $cmd = "nohup java -jar ".$path."similitud.jar ".$zip." ".$project_id." > /dev/null 2>&1 & echo $!";

        exec($cmd,$out);

        if (!isset($out[0])){

            return null;
        }

        return $out;
    }

    private function actualizaEstadoProyecto($pid,$repoid){

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        try{
            $repo = $em->getRepository('GILAppSimilitudBundle:Repositorio')->findOneBy(array(
                'idrepositorio' => $repoid,
            ));

            $repo->setUltimopid($pid);
            $repo->setFechaproceso(new \DateTime());
            $em->flush();
            $em->getConnection()->commit();

        } catch (\Exception $ex){

            $em->getConnection()->rollback();
            throw new InternalErrorException($ex->getMessage());
        }
    }

    private function deleteRepo($repoId){

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        try{

            $sqlDocs = "DELETE d,dxr FROM documento d INNER JOIN DocxRepo dxr ON d.idDocumento=dxr.Documento_idDocumento WHERE dxr.Repositorio_idRepositorio = ".$repoId;
            $sqlRepo = "DELETE r FROM repositorio r WHERE r.idRepositorio = ".$repoId;
            $sqlSim = "DELETE s FROM similitud s WHERE s.Repositorio_idRepositorio = ".$repoId;


            $stmt = $em->getConnection()->prepare($sqlDocs);
            $stmt->execute();

            $stmt = $em->getConnection()->prepare($sqlSim);
            $stmt->execute();

            $stmt = $em->getConnection()->prepare($sqlRepo);
            $stmt->execute();

            $em->getConnection()->commit();

            return true;

        } catch (\Exception $ex){

            throw new InternalErrorException($ex->getMessage()." ".$ex->getLine());
        }
    }

    private function repoExists($projectId){

        $repo = $this->getDoctrine()->getRepository('GILAppSimilitudBundle:Repositorio')->find($projectId);

        if(!$repo){

            return false;
        }
        return true;
    }
}
