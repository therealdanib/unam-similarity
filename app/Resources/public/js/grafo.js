var Grafo = function() {

    return {
        init: function (docinfo, datosinfo) {

            var doc = docinfo;
            var datos = datosinfo;
            var nodes = [];
            var edges = [];
            var network = null;

            window.onload = function() {

                datos.sort(function (a,b){
                    return b[2] - a[2];
                });

                nodes.push({
                    id:0, value: 100, label: '', title: doc[1], identifier: doc[0]
                });

                for(i=0; i<datos.length; i++)
                {

                    nodes.push({
                        id: 1+i, value: (datos[i][2]*100), label: (datos[i][2]*100).toFixed(0)+"%", title: datos[i][1], identifier: datos[i][0]
                    });

                    if(i==14) break;

                }

                for (j = 1; j<nodes.length; j++)
                {
                    edges.push({
                        from: 0, to: nodes[j].id, value: nodes[j].value, title: nodes[j].title
                    });
                }

                var container = document.getElementById('mynetwork');

                var data = {
                    nodes: nodes,
                    edges: edges
                };

                var options = {
                    width: '100%',
                    height: '350px',
                    nodes: {
                        fontColor: 'black',
                        color: {
                            background: '#3399ff',
                            border: 'grey',
                            highlight: {
                                background: 'yellow',
                                border: '#666666'
                            }
                        },
                        shape: 'dot'
                    },
                    edges: {
                        color: '#97C2FC',
                        width: 3
                    },
                    tooltip:{
                        delay:100,
                        fontColor:"black",
                        fontSize:12,
                        fontFace:"arial",
                        color:{
                            border: "#000000",
                            background:"#FFFFFF"
                        }
                    }
                };
                network = new vis.Network(container, data, options);
                network.on('doubleClick', function(properties){
                    window.location = nodes[properties.nodes].identifier;
                });
            }
        }
    }
}();