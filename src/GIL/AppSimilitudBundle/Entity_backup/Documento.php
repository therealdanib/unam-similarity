<?php

namespace GIL\AppSimilitudBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documento
 *
 * @ORM\Table(name="documento")
 * @ORM\Entity
 */
class Documento
{
    /**
     * @var string
     *
     * @ORM\Column(name="tituloDocumento", type="string", length=300, nullable=false)
     */
    private $titulodocumento;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcionDocumento", type="text", nullable=true)
     */
    private $descripciondocumento;

    /**
     * @var string
     *
     * @ORM\Column(name="rutadocumento", type="string", length=900, nullable=true)
     */
    private $rutadocumento;

    /**
     * @var string
     *
     * @ORM\Column(name="autorDocumento", type="string", length=200, nullable=true)
     */
    private $autordocumento;

    /**
     * @var integer
     *
     * @ORM\Column(name="idRepositorio", type="integer", nullable=false)
     */
    private $idrepositorio;

    /**
     * @var integer
     *
     * @ORM\Column(name="idDocumento", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $iddocumento;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="GIL\AppSimilitudBundle\Entity\Repositorio", mappedBy="documentodocumento")
     */
    private $repositoriorepositorio;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->repositoriorepositorio = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set titulodocumento
     *
     * @param string $titulodocumento
     * @return Documento
     */
    public function setTitulodocumento($titulodocumento)
    {
        $this->titulodocumento = $titulodocumento;

        return $this;
    }

    /**
     * Get titulodocumento
     *
     * @return string 
     */
    public function getTitulodocumento()
    {
        return $this->titulodocumento;
    }

    /**
     * Set descripciondocumento
     *
     * @param string $descripciondocumento
     * @return Documento
     */
    public function setDescripciondocumento($descripciondocumento)
    {
        $this->descripciondocumento = $descripciondocumento;

        return $this;
    }

    /**
     * Get descripciondocumento
     *
     * @return string 
     */
    public function getDescripciondocumento()
    {
        return $this->descripciondocumento;
    }

    /**
     * Set rutadocumento
     *
     * @param string $rutadocumento
     * @return Documento
     */
    public function setRutadocumento($rutadocumento)
    {
        $this->rutadocumento = $rutadocumento;

        return $this;
    }

    /**
     * Get rutadocumento
     *
     * @return string 
     */
    public function getRutadocumento()
    {
        return $this->rutadocumento;
    }

    /**
     * Set autordocumento
     *
     * @param string $autordocumento
     * @return Documento
     */
    public function setAutordocumento($autordocumento)
    {
        $this->autordocumento = $autordocumento;

        return $this;
    }

    /**
     * Get autordocumento
     *
     * @return string 
     */
    public function getAutordocumento()
    {
        return $this->autordocumento;
    }

    /**
     * Get iddocumento
     *
     * @return integer 
     */
    public function getIddocumento()
    {
        return $this->iddocumento;
    }

    /**
     * Add repositoriorepositorio
     *
     * @param \GIL\AppSimilitudBundle\Entity\Repositorio $repositoriorepositorio
     * @return Documento
     */
    public function addRepositoriorepositorio(\GIL\AppSimilitudBundle\Entity\Repositorio $repositoriorepositorio)
    {
        $repositoriorepositorio->addDocumentodocumento($this);
        $this->repositoriorepositorio[] = $repositoriorepositorio;

        return $this;
    }

    /**
     * Remove repositoriorepositorio
     *
     * @param \GIL\AppSimilitudBundle\Entity\Repositorio $repositoriorepositorio
     */
    public function removeRepositoriorepositorio(\GIL\AppSimilitudBundle\Entity\Repositorio $repositoriorepositorio)
    {
        $this->repositoriorepositorio->removeElement($repositoriorepositorio);
    }

    /**
     * Get repositoriorepositorio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRepositoriorepositorio()
    {
        return $this->repositoriorepositorio;
    }

    /**
     * Set iddocumento
     *
     * @param $iddocumento
     * @return Documento
     */

    public function setIddocumento($iddocumento){

        $this->iddocumento = $iddocumento;
    }

    /**
     * Set idrepositorio
     *
     * @param $idrepositorio
     * @return Documento
     */

    public function setIdrepositorio($idrepositorio){

        $this->idrepositorio = $idrepositorio;
    }

    /**
     * Get idrepositorio
     *
     * @return int
     */

    public function getIdrepositorio(){

        return $this->iddocumento;
    }

}
