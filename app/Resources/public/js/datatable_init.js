/**
 * Created by julzone on 09/11/15.
 */

var DataTable = function(){

    return{

        init: function(){

            $("#docs").DataTable({
                "responsive": true,
                "language": {
                    "info": "  _PAGE_ de _PAGES_",
                    "search":         "Buscar:",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Última",
                        "next":       ">",
                        "previous":   "<"
                    },
                    "lengthMenu":     " Mostrar _MENU_ documentos",
                    "emptyTable":     "No existen elementos en la tabla",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 elementos",
                    "infoFiltered":   "(filtrados de _MAX_ elementos)",
                    "zeroRecords":    "La búsqueda no obtuvo resultados"
                }
            });
        }
    }
}();
