<?php

namespace GIL\AppSimilitudBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Repositorio
 *
 * @ORM\Table(name="repositorio", indexes={@ORM\Index(name="fk_Repositorio_Usuario1_idx", columns={"Usuario_idUsuario"})})
 * @ORM\Entity
 */
class Repositorio
{
    /**
     * @var string
     *
     * @ORM\Column(name="tituloRepositorio", type="string", length=300, nullable=false)
     */
    private $titulorepositorio;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcionRepositorio", type="text", nullable=false)
     */
    private $descripcionrepositorio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="procesado", type="boolean", nullable=false)
     */
    private $procesado;

    /**
     * @var integer
     *
     * @ORM\Column(name="ultimoPid", type="integer", nullable=true)
     */
    private $ultimopid;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="ultimoPid", type="datetime", nullable=true)
     */
    private $fechaproceso;


    /**
     * @var integer
     *
     * @ORM\Column(name="idRepositorio", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idrepositorio;

    /**
     * @var \GIL\AppSimilitudBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="GIL\AppSimilitudBundle\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Usuario_idUsuario", referencedColumnName="idUsuario")
     * })
     */
    private $usuariousuario;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="GIL\AppSimilitudBundle\Entity\Documento", inversedBy="repositoriorepositorio")
     * @ORM\JoinTable(name="DocxRepo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="Repositorio_idRepositorio", referencedColumnName="idRepositorio")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="Documento_idDocumento", referencedColumnName="idDocumento")
     *   }
     * )
     */
    private $documentodocumento;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documentodocumento = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set titulorepositorio
     *
     * @param string $titulorepositorio
     * @return Repositorio
     */
    public function setTitulorepositorio($titulorepositorio)
    {
        $this->titulorepositorio = $titulorepositorio;

        return $this;
    }

    /**
     * Get titulorepositorio
     *
     * @return string 
     */
    public function getTitulorepositorio()
    {
        return $this->titulorepositorio;
    }

    /**
     * Set descripcionrepositorio
     *
     * @param string $descripcionrepositorio
     * @return Repositorio
     */
    public function setDescripcionrepositorio($descripcionrepositorio)
    {
        $this->descripcionrepositorio = $descripcionrepositorio;

        return $this;
    }

    /**
     * Get descripcionrepositorio
     *
     * @return string 
     */
    public function getDescripcionrepositorio()
    {
        return $this->descripcionrepositorio;
    }

    /**
     * Get idrepositorio
     *
     * @return integer 
     */
    public function getIdrepositorio()
    {
        return $this->idrepositorio;
    }

    /**
     * Set usuariousuario
     *
     * @param \GIL\AppSimilitudBundle\Entity\Usuario $usuariousuario
     * @return Repositorio
     */
    public function setUsuariousuario(\GIL\AppSimilitudBundle\Entity\Usuario $usuariousuario = null)
    {
        $this->usuariousuario = $usuariousuario;

        return $this;
    }

    /**
     * Get usuariousuario
     *
     * @return \GIL\AppSimilitudBundle\Entity\Usuario 
     */
    public function getUsuariousuario()
    {
        return $this->usuariousuario;
    }

    /**
     * Add documentodocumento
     *
     * @param \GIL\AppSimilitudBundle\Entity\Documento $documentodocumento
     * @return Repositorio
     */
    public function addDocumentodocumento(\GIL\AppSimilitudBundle\Entity\Documento $documentodocumento)
    {
        $documentodocumento->addRepositoriorepositorio($this);
        $this->documentodocumento[] = $documentodocumento;

        return $this;
    }

    /**
     * Remove documentodocumento
     *
     * @param \GIL\AppSimilitudBundle\Entity\Documento $documentodocumento
     */
    public function removeDocumentodocumento(\GIL\AppSimilitudBundle\Entity\Documento $documentodocumento)
    {
        $this->documentodocumento->removeElement($documentodocumento);
    }

    /**
     * Get documentodocumento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocumentodocumento()
    {
        return $this->documentodocumento;
    }

    /**
     * Set procesado
     *
     * @param boolean $procesado
     * @return Repositorio
     */

    public function setProcesado($procesado){

        $this->procesado = $procesado;
    }

    /**
     * Get procesado
     *
     * @return boolean
     */

    public function getProcesado(){

        return $this->procesado;
    }

    /**
     * Set ultimopid
     *
     * @param $ultimopid
     * @return Repositorio
     */

    public function setUltimopid($ultimopid){

        $this->ultimopid = $ultimopid;

    }

    /**
     * Get ultimopid
     *
     * @return int
     */

    public function getUltimopid(){

        return $this->ultimopid;
    }

    /**
     * Set fechaproceso
     *
     * @param $fechaproceso
     * @return Repositorio
     */

    public function setFechaproceso($fechaproceso){

        $this->fechaproceso = $fechaproceso;
    }

    /**
     * Get fechaproceso
     *
     * @return \Datetime
     */

    public function getFechaproceso(){

        return $this->fechaproceso;
    }

    /**
     * Set idrepositorio
     *
     * @param $idrepositorio
     * @return Repositorio
     */

    public function setIdrepositorio($idrepositorio){

        $this->idrepositorio = $idrepositorio;
    }

}
