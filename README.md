# Similarity web application
This is a repository which holds a simililarity app made by me. It's divided into two big modules:

- Java engine which calculates similarity using some Lucene libraries
- PHP project made with symfony which shows up documents, repositories and similarity results also with help from vis.js libraries and jQueryUI
