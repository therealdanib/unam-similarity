<?php
/**
 * Created by PhpStorm.
 * User: julzone
 * Date: 08/11/15
 * Time: 11:35 AM
 */

namespace GIL\AppSimilitudBundle\Controller;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


/**
 *
 * Clase para manejar diversas tareas generales de la aplicación
 * @author jbravob
 * @version 1.0
 *
 */

class Utils {


    const PARENT_URL = "http://www.corpus.unam.mx:8069";

    public static function getLogin($request) {

        $session = $request->getSession();
        $session_id = $request->cookies->get('session_id');
        $user_url = Utils::PARENT_URL."/api/user?session_id=".$session_id;
        $response = json_decode(file_get_contents($user_url, false), true);

        Utils::createSession($session,$response);
    }

    public static function clearSession($session){

        $session->invalidate();
        $session->clear();
        $session->remove('username');
        $session->remove('user_id');
        $session->remove('name');
        $session->invalidate();
        unset($session);
    }

    public static function createSession($session, $vars){

        $session->start();
        $session->set('username',$vars['login']);
        $session->set('user_id',$vars['id']);
        $session->set('name',$vars['name']);
    }
}