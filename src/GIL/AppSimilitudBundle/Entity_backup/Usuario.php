<?php

namespace GIL\AppSimilitudBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="Usuario", uniqueConstraints={@ORM\UniqueConstraint(name="idUsuario_UNIQUE", columns={"idUsuario"})})
 * @ORM\Entity
 */
class Usuario
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombreUsuario", type="string", length=100, nullable=true)
     */
    private $nombreusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoUsuario", type="string", length=100, nullable=true)
     */
    private $apellidousuario;

    /**
     * @var string
     *
     * @ORM\Column(name="idUsuario", type="string", length=15)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idusuario;



    /**
     * Set nombreusuario
     *
     * @param string $nombreusuario
     * @return Usuario
     */
    public function setNombreusuario($nombreusuario)
    {
        $this->nombreusuario = $nombreusuario;

        return $this;
    }

    /**
     * Get nombreusuario
     *
     * @return string 
     */
    public function getNombreusuario()
    {
        return $this->nombreusuario;
    }

    /**
     * Set apellidousuario
     *
     * @param string $apellidousuario
     * @return Usuario
     */
    public function setApellidousuario($apellidousuario)
    {
        $this->apellidousuario = $apellidousuario;

        return $this;
    }

    /**
     * Get apellidousuario
     *
     * @return string 
     */
    public function getApellidousuario()
    {
        return $this->apellidousuario;
    }

    /**
     * Get idusuario
     *
     * @return string 
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }
}
