<?php

namespace GIL\AppSimilitudBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * Clase principal de la aplicación
 *
 * Class DefaultController
 * @author Julio Bravo <julz@mixtlisoft.com>
 * @package GIL\AppSimilitudBundle\Controller
 */


class DefaultController extends Controller
{

    /**
     * Función principal de la aplicación.
     *
     * @param Request $request
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */

    public function indexAction(Request $request) {

        Utils::getLogin($request);
        $projects = $this->getProjectCount($request->cookies->get('session_id'));

        return $this->render('GILAppSimilitudBundle:Default:index.html.twig',array(
            'calculated' => count($this->getDoctrine()->getRepository('GILAppSimilitudBundle:Repositorio')->findAll()),
            'geco_projects' => count($projects),
            'proj_list' => $projects
        ));
    }

    /**
     * Función para terminar la sesión de la aplicación, se conecta a la API del GECO
     *
     * @param Request $request
     * @return mixed
     */

    public function logoutAction(Request $request) {

        $session = $request->getSession();
        Utils::clearSession($session);

        return $this->redirect(Utils::PARENT_URL.'/web/session/logout?redirect=/');
    }

    /**
     *
     * Función que devuelve la lista de proyectos del GECO
     *
     * @param $sessid
     * @return mixed
     */

    private function getProjectCount($sessid){

        $project_url = Utils::PARENT_URL.'/api/project?include_public=1&session_id='.$sessid;
        $response = json_decode(file_get_contents($project_url, false), true);
        return $response;

    }
}