<?php
/**
 * Created by PhpStorm.
 * User: julzone
 * Date: 27/08/15
 * Time: 10:46
 */

namespace GIL\AppSimilitudBundle\Entity;


class ApiFile
{

    private $has_children;
    private $file_id;
    private $full_name;
    private $name;
    private $type;

    public function __construct($has_children=false,$file_id, $full_name, $name , $type){

        $this->has_children = $has_children;
        $this->file_id = $file_id;
        $this->full_name = $full_name;
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getHasChildren()
    {
        return $this->has_children;
    }

    /**
     * @param mixed $has_children
     */
    public function setHasChildren($has_children)
    {
        $this->has_children = $has_children;
    }

    /**
     * @return mixed
     */
    public function getFile_id()
    {
        return $this->file_id;
    }

    /**
     * @param mixed $id
     */
    public function setFile_id($file_id)
    {
        $this->file_id = $file_id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @param mixed $full_name
     */
    public function setFullName($full_name)
    {
        $this->full_name = $full_name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}