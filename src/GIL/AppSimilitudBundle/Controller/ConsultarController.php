<?php

namespace GIL\AppSimilitudBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\DBAL\Driver\PDOMySql;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 *
 * Clase que contiene la lógica para las consultas de repositorios
 *
 * Class ConsultarController
 * @author Julio Bravo <julz@mixtlisoft.com>
 * @package GIL\AppSimilitudBundle\Controller
 */
class ConsultarController extends Controller
{

    /**
     *
     * Método para hacer el render a la parte de consulta de repositorios con similitud calculada.
     *
     * @param Request $request "Petición http"
     * @return mixed
     */
    public function consultarAction(Request $request) {

        Utils::getLogin($request);
        $repos = $this->getDoctrine()->getRepository('GILAppSimilitudBundle:Repositorio')->findAll();
        return $this->render('GILAppSimilitudBundle:Default:consultar.html.twig',array('repos'=>$repos));
    }

    /**
     *
     * Método que hace el render para consultar un repositorio y sus documentos.
     * En caso de que la similitud del mismo no haya sido calculada, se redirige al método para calcularla.
     *
     * @param Request $request "Petición http"
     * @return mixed
     */
    public function repoAction(Request $request) {

        Utils::getLogin($request);
        $id = $request->query->get('pid');
        $iframe = $request->query->get('embed');

        $repo = $this->getRepo($id);

        if(!$repo){

            return $this->redirectToRoute('_create',array(
                'pid' => $id,
                'embed'=>$iframe ? $iframe : 0
            ));
        }

        $docsq = $this->getDocumentos($repo->getIdrepositorio());

        $docs = array();

        foreach ($docsq as $doc) {

            $doc['descripciondocumento'] = json_decode(utf8_encode($doc['descripciondocumento']),true);
            array_push($docs, $doc);
        }

        return $this->render('GILAppSimilitudBundle:Default:repo.html.twig',
            array(
                'iframe'=>$iframe,
                'docs' => $docs,
                'metadata_headers'=>array_keys($docs[0]['descripciondocumento']),
                'describerepo'=>$repo->getDescripcionrepositorio(),
                'titulorepo'=>$repo->getTitulorepositorio(),
                'idrepo'=>$repo->getIdrepositorio(),
                'procId'=>$repo->getUltimopid()
            )
        );
    }

    /**
     * Método para obtener el contenido de un documento, así como sus documentos similares a través del grafo.
     *
     * @param $idrepo "identificador del repositorio"
     * @param $idd "Identificador del documento"
     * @param $iframe "Bandera que indica si el documento debe verse embebido o no"
     * @param Request $request "Petición http"
     * @return mixed
     */

    public function docAction($idrepo, $idd, $iframe, Request $request) {

        Utils::getLogin($request);
        $repo = $this->getRepo($idrepo);
        $documento = $this->getDocumento($idd);
        $params = get_object_vars(json_decode($documento[2],false));

        return $this->render('GILAppSimilitudBundle:Default:doc.html.twig',
            array(
                'iframe' => $iframe,
                'documento'=>$documento,
                'describerepo'=>$repo->getDescripcionrepositorio(),
                'titulorepo'=>$repo->getTitulorepositorio(),
                'idrepo'=>$repo->getIdrepositorio(),
                'results_json'=>$this->getSimilarDocs($idd,$idrepo),
                'params' => $params,
                'content' => $this->getContent($request->cookies->get('session_id'),$idd)
            )
        );
    }

    /**
     *
     * Método privado para obtener documentos similares a otro de la BD
     *
     * @param $idd "Id del documento a comparar"
     * @param $idrepo "Id del repositorio del cual es el documento"
     * @return array "Arreglo con los documentos similares"
     */

    private function getSimilarDocs($idd,$idrepo) {

        $results = array();

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $similar_docs = $qb
            ->select('
        CASE WHEN (s.iddoc1 != :idd) THEN s.iddoc1 ELSE s.iddoc2 END as iddoc,
        d.titulodocumento,
        s.valorcoseno')
            ->from('GILAppSimilitudBundle:Similitud', 's')
            ->innerJoin('GILAppSimilitudBundle:Documento', 'd', 'WITH', 'd.iddocumento != :idd AND (d.iddocumento = s.iddoc1 OR d.iddocumento = s.iddoc2)')
            ->where('s.iddoc1 = :idd OR s.iddoc2 = :idd')
            ->andWhere('s.repositorioIdrepositorio = :idrepo')
            ->setParameters(array(
                'idd' => $idd,
                'idrepo' => $idrepo
            ))
            ->getQuery()
            ->getResult();

        foreach ($similar_docs as $d) {

            array_push($results,array(
                $d['iddoc'],
                $d['titulodocumento'],
                $d['valorcoseno']
            ));
        }
        return $results;
    }

    /**
     *
     * Obtención del repositorio de documentos dado su id
     *
     * @param $id "Id del repositorio o proyecto"
     * @return mixed "Devuelve un objeto de la clase Repositorio"
     */

    private function getRepo($id) {

        return $this->getDoctrine()
            ->getRepository('GILAppSimilitudBundle:Repositorio')
            ->find($id);
    }

    /**
     *
     * Obtención de un documento dado su id
     *
     * @param $id "Identificador del documento"
     * @return mixed "Devuelve un objeto de la clase Documento"
     */

    private function getDocumento($id) {

        $d = $this->getDoctrine()

            ->getRepository('GILAppSimilitudBundle:Documento')
            ->findOneBy(array(
                'iddocumento' => $id
            ));

        return array(

            $d->getIddocumento(),
            $d->getTitulodocumento(),
            $d->getDescripciondocumento(),
            $d->getRutadocumento(),
            $d->getAutordocumento()
        );
    }

    /**
     *
     * Obtiene un arreglo de documentos dado un id de repositorio
     *
     * @param $repoId "Id del repositorio"
     * @return mixed "Arreglo de documentos"
     *
     */

    private function getDocumentos($repoId){

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $query = $qb->select("d.iddocumento, d.titulodocumento, d.descripciondocumento, d.rutadocumento, d.autordocumento")
            ->from('GILAppSimilitudBundle:Documento','d')
            ->innerJoin('GILAppSimilitudBundle:Docxrepo','dxr','WITH','d.iddocumento = dxr.documentodocumento AND dxr.repositoriorepositorio = :repoid')
            ->orderBy('d.iddocumento')
            ->setParameter('repoid',$repoId)
            ->getQuery()
            ->getResult();

        return $query;
    }

    /**
     *
     * Función que ejecuta un comando en linux para saber si un proceso sigue vivo
     *
     * @param $pid "Id del proceso"
     * @return bool "Devuelve cierto o falso depende de si el proceso sigue vivo"
     */

    private function isPidAlive($pid){

        $cmd = 'ps -p '.$pid;
        exec($cmd,$res);

        if(isset($res[1])){

            return true;
        }

        return false;
    }

    public function checkpidAction(Request $request){

        $pid = $request->query->get('pid');
        $ps = false;

        $cmd = 'ps -p '.$pid;
        exec($cmd,$res);

        $response = new JsonResponse();

        if(isset($res[1])){

            $ps = array_values(array_filter(explode(' ',$res[1]), function($ar){ return ($ar !== '' && $ar !== '?'); }));
        }

        $response->setData(array(
            'alive'=> $ps
        ));

        return $response;
    }

    private function getContent($session_id, $iddoc){

        $files_url = Utils::PARENT_URL.'/api/download?id='.$iddoc.'&session_id='.$session_id;
        return file_get_contents($files_url);
    }
}
