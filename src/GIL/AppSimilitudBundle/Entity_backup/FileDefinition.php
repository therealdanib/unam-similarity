<?php

namespace GIL\AppSimilitudBundle\Entity;

class FileDefinition {

    private $name;
    private $path;
    private $class;
    private $parent_dir;

    public function __construct($name,$path, $type, $parent_dir=""){

        $this->name = $name;
        $this->path = $path;
        $this->class = $type;
        $this->parent_dir = $parent_dir;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getParent_dir()
    {
        return $this->parent_dir;
    }
}