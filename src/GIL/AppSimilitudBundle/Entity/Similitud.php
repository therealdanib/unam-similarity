<?php

namespace GIL\AppSimilitudBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Similitud
 *
 * @ORM\Table(name="similitud", indexes={@ORM\Index(name="fk_Similitud_Repositorio1_idx", columns={"Repositorio_idRepositorio"})})
 * @ORM\Entity
 */
class Similitud
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Repositorio_idRepositorio", type="integer", nullable=false)
     */
    private $repositorioIdrepositorio;

    /**
     * @var integer
     *
     * @ORM\Column(name="idDoc1", type="integer", nullable=false)
     */
    private $iddoc1;

    /**
     * @var integer
     *
     * @ORM\Column(name="idDoc2", type="integer", nullable=false)
     */
    private $iddoc2;

    /**
     * @var string
     *
     * @ORM\Column(name="valorcoseno", type="string", length=20, nullable=true)
     */
    private $valorcoseno;

    /**
     * @var integer
     *
     * @ORM\Column(name="valorlevenshtein", type="integer", nullable=true)
     */
    private $valorlevenshtein;

    /**
     * @var integer
     *
     * @ORM\Column(name="idsim", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idsim;



    /**
     * Set repositorioIdrepositorio
     *
     * @param integer $repositorioIdrepositorio
     * @return Similitud
     */
    public function setRepositorioIdrepositorio($repositorioIdrepositorio)
    {
        $this->repositorioIdrepositorio = $repositorioIdrepositorio;

        return $this;
    }

    /**
     * Get repositorioIdrepositorio
     *
     * @return integer 
     */
    public function getRepositorioIdrepositorio()
    {
        return $this->repositorioIdrepositorio;
    }

    /**
     * Set iddoc1
     *
     * @param integer $iddoc1
     * @return Similitud
     */
    public function setIddoc1($iddoc1)
    {
        $this->iddoc1 = $iddoc1;

        return $this;
    }

    /**
     * Get iddoc1
     *
     * @return integer 
     */
    public function getIddoc1()
    {
        return $this->iddoc1;
    }

    /**
     * Set iddoc2
     *
     * @param integer $iddoc2
     * @return Similitud
     */
    public function setIddoc2($iddoc2)
    {
        $this->iddoc2 = $iddoc2;

        return $this;
    }

    /**
     * Get iddoc2
     *
     * @return integer 
     */
    public function getIddoc2()
    {
        return $this->iddoc2;
    }

    /**
     * Set valorcoseno
     *
     * @param string $valorcoseno
     * @return Similitud
     */
    public function setValorcoseno($valorcoseno)
    {
        $this->valorcoseno = $valorcoseno;

        return $this;
    }

    /**
     * Get valorcoseno
     *
     * @return string 
     */
    public function getValorcoseno()
    {
        return $this->valorcoseno;
    }

    /**
     * Set valorlevenshtein
     *
     * @param integer $valorlevenshtein
     * @return Similitud
     */
    public function setValorlevenshtein($valorlevenshtein)
    {
        $this->valorlevenshtein = $valorlevenshtein;

        return $this;
    }

    /**
     * Get valorlevenshtein
     *
     * @return integer 
     */
    public function getValorlevenshtein()
    {
        return $this->valorlevenshtein;
    }

    /**
     * Get idsim
     *
     * @return integer 
     */
    public function getIdsim()
    {
        return $this->idsim;
    }
}
